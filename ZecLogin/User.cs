﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;


namespace Entities.Zeclogin
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public string Email { get;  set; }
        public string Salt { get; set; }

        public Country Country { get; set; }
        public Avatar Avatar { get; set; }


        public User()
        {
            this.Role = Entities.Role.User;
        }
    }
}
