﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Role
    {
        public const string Admin = "Admin";
        public const string Host = "Host";
        public const string ModRifa = "ModRifa";
        public const string ModPart = "ModPart";
        public const string User = "User";
        public const string Supp = "Support";
        public const string Micro = "Micro";
    }
}
